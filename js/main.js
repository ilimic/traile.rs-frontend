jQuery( document ).ready(function($) {
	var mySwiper = new Swiper('header .swiper-container',{
		pagination: 'header .pagination',
		loop:true,
		grabCursor: true,
		paginationClickable: true
	})
	$('header .arrow-left').on('click', function(e){
	      e.preventDefault()
	      mySwiper.swipePrev()
	})
	$('header .arrow-right').on('click', function(e){
	      e.preventDefault()
	      mySwiper.swipeNext()
	})

	var movieposterRowSwiper = new Swiper('.movieposterrow .swiper-container',{
	      grabCursor: true,
	      slidesPerView: 'auto'
	})
	$('.movieposterrow .arrow-left').on('click', function(e){
	      e.preventDefault()
	      movieposterRowSwiper.swipePrev()
	})
	$('.movieposterrow .arrow-right').on('click', function(e){
	      e.preventDefault()
	      movieposterRowSwiper.swipeNext()
	})

	$(".movieposter").hover(function() {
	      $(this).toggleClass("show-playicon");
	});

	/*$('body').popover({
		selector: '.movieposter',
		trigger: 'hover',
		container: 'body',
		placement: 'auto',
		delay: {show: 400, hide: 0},
		animation: true,
		html: true
	});*/
});